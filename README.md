# **DiscCat**
## Software de catálogo de CDs
---

Projecto efectuado no âmbito da Prova de Aptidão Tecnológica do Curso Tecnológico de Informática


![disccat.png](https://i.imgur.com/NwV7Nx7.png)


### Instalação ###
Para instalar, basta correr o ficheiro Setup_DiscCat.exe e seguir as instruções.
É necessário ter uma versão do Microsoft SQL Server 2005 instalado para correr a aplicação.

### Como utilizar a aplicação? ###
Existe um manual em formato HTML na mesma pasta onde se encontra o executável da aplicação.