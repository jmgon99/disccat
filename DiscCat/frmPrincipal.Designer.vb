<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmPrincipal
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmPrincipal))
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.txtProcurar = New System.Windows.Forms.TextBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.SplitContainer1 = New System.Windows.Forms.SplitContainer
        Me.lstDisco = New System.Windows.Forms.ListView
        Me.treeEstrutura = New System.Windows.Forms.TreeView
        Me.Panel2 = New System.Windows.Forms.Panel
        Me.Panel3 = New System.Windows.Forms.Panel
        Me.txtNomeEmprestado = New System.Windows.Forms.TextBox
        Me.Label8 = New System.Windows.Forms.Label
        Me.Label7 = New System.Windows.Forms.Label
        Me.txtDadosEmprestado = New System.Windows.Forms.TextBox
        Me.chkEmprestado = New System.Windows.Forms.CheckBox
        Me.txtImagem = New System.Windows.Forms.TextBox
        Me.btnImagem = New System.Windows.Forms.Button
        Me.picImagem = New System.Windows.Forms.PictureBox
        Me.cmbCategoria = New System.Windows.Forms.ComboBox
        Me.txtDescricao = New System.Windows.Forms.TextBox
        Me.txtEtiqueta = New System.Windows.Forms.TextBox
        Me.Label5 = New System.Windows.Forms.Label
        Me.Label4 = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip
        Me.FicheiroToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem
        Me.MostrarTodasAsDrivesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripSeparator2 = New System.Windows.Forms.ToolStripSeparator
        Me.SairToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.AjudaToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem
        Me.Conte˙dosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.SobreOProgramaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.FicheiroToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.menuMostrarDiscos = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator
        Me.SaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.AjudaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.═ndiceDaAjudaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.SobreToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.btnInserir = New System.Windows.Forms.Button
        Me.dialogImagem = New System.Windows.Forms.OpenFileDialog
        Me.btnApagar = New System.Windows.Forms.Button
        Me.cmbDrives = New System.Windows.Forms.ComboBox
        Me.Label6 = New System.Windows.Forms.Label
        Me.ImageList1 = New System.Windows.Forms.ImageList(Me.components)
        Me.Panel1.SuspendLayout()
        Me.SplitContainer1.Panel1.SuspendLayout()
        Me.SplitContainer1.Panel2.SuspendLayout()
        Me.SplitContainer1.SuspendLayout()
        Me.Panel2.SuspendLayout()
        Me.Panel3.SuspendLayout()
        CType(Me.picImagem, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.MenuStrip1.SuspendLayout()
        Me.SuspendLayout()
        '
        'Panel1
        '
        Me.Panel1.AutoScroll = True
        Me.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel1.Controls.Add(Me.txtProcurar)
        Me.Panel1.Controls.Add(Me.Label1)
        Me.Panel1.Location = New System.Drawing.Point(628, 27)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(159, 65)
        Me.Panel1.TabIndex = 0
        '
        'txtProcurar
        '
        Me.txtProcurar.Location = New System.Drawing.Point(12, 34)
        Me.txtProcurar.Name = "txtProcurar"
        Me.txtProcurar.Size = New System.Drawing.Size(132, 20)
        Me.txtProcurar.TabIndex = 1
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(45, 9)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(66, 18)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Pesquisa:"
        '
        'SplitContainer1
        '
        Me.SplitContainer1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.SplitContainer1.Location = New System.Drawing.Point(12, 232)
        Me.SplitContainer1.Name = "SplitContainer1"
        '
        'SplitContainer1.Panel1
        '
        Me.SplitContainer1.Panel1.Controls.Add(Me.lstDisco)
        '
        'SplitContainer1.Panel2
        '
        Me.SplitContainer1.Panel2.Controls.Add(Me.treeEstrutura)
        Me.SplitContainer1.Size = New System.Drawing.Size(775, 280)
        Me.SplitContainer1.SplitterDistance = 492
        Me.SplitContainer1.TabIndex = 1
        '
        'lstDisco
        '
        Me.lstDisco.Activation = System.Windows.Forms.ItemActivation.OneClick
        Me.lstDisco.BackColor = System.Drawing.Color.AliceBlue
        Me.lstDisco.BackgroundImageTiled = True
        Me.lstDisco.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lstDisco.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lstDisco.FullRowSelect = True
        Me.lstDisco.GridLines = True
        Me.lstDisco.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable
        Me.lstDisco.Location = New System.Drawing.Point(0, 0)
        Me.lstDisco.MultiSelect = False
        Me.lstDisco.Name = "lstDisco"
        Me.lstDisco.Size = New System.Drawing.Size(492, 280)
        Me.lstDisco.TabIndex = 0
        Me.lstDisco.UseCompatibleStateImageBehavior = False
        Me.lstDisco.View = System.Windows.Forms.View.Details
        '
        'treeEstrutura
        '
        Me.treeEstrutura.BackColor = System.Drawing.Color.AliceBlue
        Me.treeEstrutura.Dock = System.Windows.Forms.DockStyle.Fill
        Me.treeEstrutura.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.treeEstrutura.Location = New System.Drawing.Point(0, 0)
        Me.treeEstrutura.Name = "treeEstrutura"
        Me.treeEstrutura.Size = New System.Drawing.Size(279, 280)
        Me.treeEstrutura.TabIndex = 0
        '
        'Panel2
        '
        Me.Panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel2.Controls.Add(Me.Panel3)
        Me.Panel2.Controls.Add(Me.txtImagem)
        Me.Panel2.Controls.Add(Me.btnImagem)
        Me.Panel2.Controls.Add(Me.picImagem)
        Me.Panel2.Controls.Add(Me.cmbCategoria)
        Me.Panel2.Controls.Add(Me.txtDescricao)
        Me.Panel2.Controls.Add(Me.txtEtiqueta)
        Me.Panel2.Controls.Add(Me.Label5)
        Me.Panel2.Controls.Add(Me.Label4)
        Me.Panel2.Controls.Add(Me.Label3)
        Me.Panel2.Controls.Add(Me.Label2)
        Me.Panel2.Location = New System.Drawing.Point(12, 27)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(610, 199)
        Me.Panel2.TabIndex = 2
        '
        'Panel3
        '
        Me.Panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel3.Controls.Add(Me.txtNomeEmprestado)
        Me.Panel3.Controls.Add(Me.Label8)
        Me.Panel3.Controls.Add(Me.Label7)
        Me.Panel3.Controls.Add(Me.txtDadosEmprestado)
        Me.Panel3.Controls.Add(Me.chkEmprestado)
        Me.Panel3.Location = New System.Drawing.Point(243, 11)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(193, 120)
        Me.Panel3.TabIndex = 17
        '
        'txtNomeEmprestado
        '
        Me.txtNomeEmprestado.Enabled = False
        Me.txtNomeEmprestado.Location = New System.Drawing.Point(51, 31)
        Me.txtNomeEmprestado.Name = "txtNomeEmprestado"
        Me.txtNomeEmprestado.Size = New System.Drawing.Size(132, 20)
        Me.txtNomeEmprestado.TabIndex = 11
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Arial Unicode MS", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Location = New System.Drawing.Point(6, 67)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(37, 16)
        Me.Label8.TabIndex = 16
        Me.Label8.Text = "Nota:"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(6, 31)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(39, 18)
        Me.Label7.TabIndex = 5
        Me.Label7.Text = "Para:"
        '
        'txtDadosEmprestado
        '
        Me.txtDadosEmprestado.Enabled = False
        Me.txtDadosEmprestado.Location = New System.Drawing.Point(51, 66)
        Me.txtDadosEmprestado.Multiline = True
        Me.txtDadosEmprestado.Name = "txtDadosEmprestado"
        Me.txtDadosEmprestado.Size = New System.Drawing.Size(132, 45)
        Me.txtDadosEmprestado.TabIndex = 12
        '
        'chkEmprestado
        '
        Me.chkEmprestado.AutoSize = True
        Me.chkEmprestado.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.chkEmprestado.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkEmprestado.Location = New System.Drawing.Point(3, 3)
        Me.chkEmprestado.Name = "chkEmprestado"
        Me.chkEmprestado.Size = New System.Drawing.Size(104, 22)
        Me.chkEmprestado.TabIndex = 13
        Me.chkEmprestado.Text = "Emprestado?"
        Me.chkEmprestado.UseVisualStyleBackColor = True
        '
        'txtImagem
        '
        Me.txtImagem.Location = New System.Drawing.Point(14, 168)
        Me.txtImagem.Name = "txtImagem"
        Me.txtImagem.Size = New System.Drawing.Size(344, 20)
        Me.txtImagem.TabIndex = 15
        '
        'btnImagem
        '
        Me.btnImagem.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnImagem.Location = New System.Drawing.Point(361, 166)
        Me.btnImagem.Name = "btnImagem"
        Me.btnImagem.Size = New System.Drawing.Size(77, 22)
        Me.btnImagem.TabIndex = 14
        Me.btnImagem.Text = "Escolher..."
        Me.btnImagem.UseVisualStyleBackColor = True
        '
        'picImagem
        '
        Me.picImagem.BackColor = System.Drawing.Color.AliceBlue
        Me.picImagem.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.picImagem.Cursor = System.Windows.Forms.Cursors.Hand
        Me.picImagem.ErrorImage = Nothing
        Me.picImagem.ImageLocation = ""
        Me.picImagem.InitialImage = Nothing
        Me.picImagem.Location = New System.Drawing.Point(444, 11)
        Me.picImagem.Name = "picImagem"
        Me.picImagem.Size = New System.Drawing.Size(155, 155)
        Me.picImagem.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.picImagem.TabIndex = 10
        Me.picImagem.TabStop = False
        '
        'cmbCategoria
        '
        Me.cmbCategoria.FormattingEnabled = True
        Me.cmbCategoria.Items.AddRange(New Object() {"Filmes", "Jogos", "Programas", "M˙sica", "Vßrios"})
        Me.cmbCategoria.Location = New System.Drawing.Point(79, 121)
        Me.cmbCategoria.Name = "cmbCategoria"
        Me.cmbCategoria.Size = New System.Drawing.Size(125, 21)
        Me.cmbCategoria.TabIndex = 9
        '
        'txtDescricao
        '
        Me.txtDescricao.Location = New System.Drawing.Point(79, 46)
        Me.txtDescricao.Multiline = True
        Me.txtDescricao.Name = "txtDescricao"
        Me.txtDescricao.Size = New System.Drawing.Size(158, 60)
        Me.txtDescricao.TabIndex = 8
        '
        'txtEtiqueta
        '
        Me.txtEtiqueta.Location = New System.Drawing.Point(79, 11)
        Me.txtEtiqueta.Name = "txtEtiqueta"
        Me.txtEtiqueta.Size = New System.Drawing.Size(158, 20)
        Me.txtEtiqueta.TabIndex = 7
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Arial Unicode MS", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(462, 169)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(118, 16)
        Me.Label5.TabIndex = 3
        Me.Label5.Text = "Imagem do disco"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(6, 122)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(67, 18)
        Me.Label4.TabIndex = 2
        Me.Label4.Text = "Categoria:"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(3, 46)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(70, 18)
        Me.Label3.TabIndex = 1
        Me.Label3.Text = "DescrišŃo:"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Arial Unicode MS", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(3, 10)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(60, 18)
        Me.Label2.TabIndex = 0
        Me.Label2.Text = "Etiqueta:"
        '
        'MenuStrip1
        '
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.FicheiroToolStripMenuItem1, Me.AjudaToolStripMenuItem1})
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.Size = New System.Drawing.Size(799, 24)
        Me.MenuStrip1.TabIndex = 3
        Me.MenuStrip1.Text = "MenuStrip1"
        '
        'FicheiroToolStripMenuItem1
        '
        Me.FicheiroToolStripMenuItem1.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.MostrarTodasAsDrivesToolStripMenuItem, Me.ToolStripSeparator2, Me.SairToolStripMenuItem})
        Me.FicheiroToolStripMenuItem1.Name = "FicheiroToolStripMenuItem1"
        Me.FicheiroToolStripMenuItem1.Size = New System.Drawing.Size(56, 20)
        Me.FicheiroToolStripMenuItem1.Text = "&Ficheiro"
        '
        'MostrarTodasAsDrivesToolStripMenuItem
        '
        Me.MostrarTodasAsDrivesToolStripMenuItem.Name = "MostrarTodasAsDrivesToolStripMenuItem"
        Me.MostrarTodasAsDrivesToolStripMenuItem.Size = New System.Drawing.Size(198, 22)
        Me.MostrarTodasAsDrivesToolStripMenuItem.Text = "Mostrar todas as drives"
        '
        'ToolStripSeparator2
        '
        Me.ToolStripSeparator2.Name = "ToolStripSeparator2"
        Me.ToolStripSeparator2.Size = New System.Drawing.Size(195, 6)
        '
        'SairToolStripMenuItem
        '
        Me.SairToolStripMenuItem.Name = "SairToolStripMenuItem"
        Me.SairToolStripMenuItem.Size = New System.Drawing.Size(198, 22)
        Me.SairToolStripMenuItem.Text = "&Sair"
        '
        'AjudaToolStripMenuItem1
        '
        Me.AjudaToolStripMenuItem1.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.Conte˙dosToolStripMenuItem, Me.SobreOProgramaToolStripMenuItem})
        Me.AjudaToolStripMenuItem1.Name = "AjudaToolStripMenuItem1"
        Me.AjudaToolStripMenuItem1.Size = New System.Drawing.Size(47, 20)
        Me.AjudaToolStripMenuItem1.Text = "&Ajuda"
        '
        'Conte˙dosToolStripMenuItem
        '
        Me.Conte˙dosToolStripMenuItem.Name = "Conte˙dosToolStripMenuItem"
        Me.Conte˙dosToolStripMenuItem.Size = New System.Drawing.Size(171, 22)
        Me.Conte˙dosToolStripMenuItem.Text = "Ajuda"
        '
        'SobreOProgramaToolStripMenuItem
        '
        Me.SobreOProgramaToolStripMenuItem.Name = "SobreOProgramaToolStripMenuItem"
        Me.SobreOProgramaToolStripMenuItem.Size = New System.Drawing.Size(171, 22)
        Me.SobreOProgramaToolStripMenuItem.Text = "Sobre o programa"
        '
        'FicheiroToolStripMenuItem
        '
        Me.FicheiroToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.menuMostrarDiscos, Me.ToolStripSeparator1, Me.SaToolStripMenuItem})
        Me.FicheiroToolStripMenuItem.Name = "FicheiroToolStripMenuItem"
        Me.FicheiroToolStripMenuItem.Size = New System.Drawing.Size(56, 20)
        Me.FicheiroToolStripMenuItem.Text = "&Ficheiro"
        '
        'menuMostrarDiscos
        '
        Me.menuMostrarDiscos.Name = "menuMostrarDiscos"
        Me.menuMostrarDiscos.Size = New System.Drawing.Size(198, 22)
        Me.menuMostrarDiscos.Text = "Mostrar todas as drives"
        '
        'ToolStripSeparator1
        '
        Me.ToolStripSeparator1.Name = "ToolStripSeparator1"
        Me.ToolStripSeparator1.Size = New System.Drawing.Size(195, 6)
        '
        'SaToolStripMenuItem
        '
        Me.SaToolStripMenuItem.Name = "SaToolStripMenuItem"
        Me.SaToolStripMenuItem.Size = New System.Drawing.Size(198, 22)
        Me.SaToolStripMenuItem.Text = "Sair"
        '
        'AjudaToolStripMenuItem
        '
        Me.AjudaToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.═ndiceDaAjudaToolStripMenuItem, Me.SobreToolStripMenuItem})
        Me.AjudaToolStripMenuItem.Name = "AjudaToolStripMenuItem"
        Me.AjudaToolStripMenuItem.Size = New System.Drawing.Size(47, 20)
        Me.AjudaToolStripMenuItem.Text = "&Ajuda"
        '
        '═ndiceDaAjudaToolStripMenuItem
        '
        Me.═ndiceDaAjudaToolStripMenuItem.Name = "═ndiceDaAjudaToolStripMenuItem"
        Me.═ndiceDaAjudaToolStripMenuItem.Size = New System.Drawing.Size(159, 22)
        Me.═ndiceDaAjudaToolStripMenuItem.Text = "═ndice da ajuda"
        '
        'SobreToolStripMenuItem
        '
        Me.SobreToolStripMenuItem.Name = "SobreToolStripMenuItem"
        Me.SobreToolStripMenuItem.Size = New System.Drawing.Size(159, 22)
        Me.SobreToolStripMenuItem.Text = "Sobre..."
        '
        'btnInserir
        '
        Me.btnInserir.Font = New System.Drawing.Font("Arial", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnInserir.Location = New System.Drawing.Point(641, 159)
        Me.btnInserir.Name = "btnInserir"
        Me.btnInserir.Size = New System.Drawing.Size(125, 27)
        Me.btnInserir.TabIndex = 4
        Me.btnInserir.Text = "Inserir Disco"
        Me.btnInserir.UseVisualStyleBackColor = True
        '
        'dialogImagem
        '
        Me.dialogImagem.FilterIndex = 0
        Me.dialogImagem.InitialDirectory = "c:"
        '
        'btnApagar
        '
        Me.btnApagar.Font = New System.Drawing.Font("Arial", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnApagar.Location = New System.Drawing.Point(641, 192)
        Me.btnApagar.Name = "btnApagar"
        Me.btnApagar.Size = New System.Drawing.Size(125, 27)
        Me.btnApagar.TabIndex = 5
        Me.btnApagar.Text = "Apagar Disco"
        Me.btnApagar.UseVisualStyleBackColor = True
        '
        'cmbDrives
        '
        Me.cmbDrives.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbDrives.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbDrives.FormattingEnabled = True
        Me.cmbDrives.Location = New System.Drawing.Point(628, 120)
        Me.cmbDrives.Name = "cmbDrives"
        Me.cmbDrives.Size = New System.Drawing.Size(158, 23)
        Me.cmbDrives.TabIndex = 7
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Arial", 9.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(646, 102)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(121, 15)
        Me.Label6.TabIndex = 8
        Me.Label6.Text = "Escolha uma Drive..."
        '
        'ImageList1
        '
        Me.ImageList1.ColorDepth = System.Windows.Forms.ColorDepth.Depth8Bit
        Me.ImageList1.ImageSize = New System.Drawing.Size(16, 16)
        Me.ImageList1.TransparentColor = System.Drawing.Color.Transparent
        '
        'frmPrincipal
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.ClientSize = New System.Drawing.Size(799, 520)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.btnApagar)
        Me.Controls.Add(Me.btnInserir)
        Me.Controls.Add(Me.cmbDrives)
        Me.Controls.Add(Me.Panel2)
        Me.Controls.Add(Me.SplitContainer1)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.MenuStrip1)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MainMenuStrip = Me.MenuStrip1
        Me.Name = "frmPrincipal"
        Me.Text = "DiscCat"
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.SplitContainer1.Panel1.ResumeLayout(False)
        Me.SplitContainer1.Panel2.ResumeLayout(False)
        Me.SplitContainer1.ResumeLayout(False)
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        Me.Panel3.ResumeLayout(False)
        Me.Panel3.PerformLayout()
        CType(Me.picImagem, System.ComponentModel.ISupportInitialize).EndInit()
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents txtProcurar As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents SplitContainer1 As System.Windows.Forms.SplitContainer
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents MenuStrip1 As System.Windows.Forms.MenuStrip
    Friend WithEvents FicheiroToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents SaToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents AjudaToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ═ndiceDaAjudaToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents SobreToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents cmbCategoria As System.Windows.Forms.ComboBox
    Friend WithEvents txtDescricao As System.Windows.Forms.TextBox
    Friend WithEvents txtEtiqueta As System.Windows.Forms.TextBox
    Friend WithEvents picImagem As System.Windows.Forms.PictureBox
    Friend WithEvents txtNomeEmprestado As System.Windows.Forms.TextBox
    Friend WithEvents txtDadosEmprestado As System.Windows.Forms.TextBox
    Friend WithEvents chkEmprestado As System.Windows.Forms.CheckBox
    Friend WithEvents btnInserir As System.Windows.Forms.Button
    Friend WithEvents btnImagem As System.Windows.Forms.Button
    Friend WithEvents treeEstrutura As System.Windows.Forms.TreeView
    Friend WithEvents dialogImagem As System.Windows.Forms.OpenFileDialog
    Friend WithEvents txtImagem As System.Windows.Forms.TextBox
    Friend WithEvents lstDisco As System.Windows.Forms.ListView
    Friend WithEvents btnApagar As System.Windows.Forms.Button
    Friend WithEvents cmbDrives As System.Windows.Forms.ComboBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents menuMostrarDiscos As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents FicheiroToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents MostrarTodasAsDrivesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator2 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents SairToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents AjudaToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents Conte˙dosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents SobreOProgramaToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ImageList1 As System.Windows.Forms.ImageList

End Class
