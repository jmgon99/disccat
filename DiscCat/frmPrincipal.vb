Imports System.IO
Imports System.Data.DataRow
Imports System.Data
Imports System.Data.SqlClient
Imports System.Drawing.Imaging
Imports System.Xml

Public Class frmPrincipal

    Dim i As Integer
    Dim numlinhas As Integer
    Dim strProcurar As String
    Dim file As IO.File
    Dim existeImg As Boolean = False
    Dim semImg As Image
    Dim strCodDisco As String = Nothing
    Dim strNome As String = Nothing
    Dim SQL As New SqlClient.SqlConnection("Data Source=.\SQLEXPRESS;AttachDbFilename=|DataDirectory|\bd_discos.mdf;Integrated Security=True;User Instance=True")
    Dim letraDrive As String = Nothing
    Dim ds As DataSet
    Dim da As SqlDataAdapter
    Dim dirImg As String = Application.StartupPath & "\Imagens\"
    Dim dirXml As String = Application.StartupPath & "\Xml\"
    Dim xmlDoc As New XmlDocument
    Dim oElmntRoot As XmlElement


#Region " Menu "

    Private Sub MostrarTodasAsDrivesToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MostrarTodasAsDrivesToolStripMenuItem.Click

        Select Case MostrarTodasAsDrivesToolStripMenuItem.Checked
            Case "True"
                MostrarTodasAsDrivesToolStripMenuItem.Checked = False
            Case "False"
                MostrarTodasAsDrivesToolStripMenuItem.Checked = True
        End Select

        infoDrives()

    End Sub

    Private Sub SairToolStripMenuItem_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SairToolStripMenuItem.Click

        Me.Dispose()

    End Sub

    Private Sub SobreOProgramaToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SobreOProgramaToolStripMenuItem.Click
        formSobre.Show()
    End Sub

    Private Sub Conte�dosToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Conte�dosToolStripMenuItem.Click

        Diagnostics.Process.Start(Application.StartupPath & "\manual.html")

    End Sub

#End Region


#Region " Subs "

    Private Sub frmPrincipal_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        'criar o direct�rio que ter� as imagens
        If Directory.Exists(Application.StartupPath & "\Imagens") = False Then
            Directory.CreateDirectory(Application.StartupPath & "\Imagens")
        End If

        'criar o direct�rio que ter� os ficheiros XML
        If Directory.Exists(Application.StartupPath & "\XML") = False Then
            Directory.CreateDirectory(Application.StartupPath & "\XML")
        End If

        ds = New DataSet
        semImg = Image.FromFile(Application.StartupPath & "\Imagens\semimagem.jpg")
        infoDrives()
        ListarDados()

    End Sub

    Private Sub btnImagem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnImagem.Click

        With dialogImagem
            .InitialDirectory = "C:\"
            .Filter = "Todos os ficheiros|*.*|Ficheiros de Mapa de Bits|*.bmp|Imagens GIF|*.gif|Imagens PNG|*.png|Imagens JPEG|*.jpg"
            .FilterIndex = 5
        End With

        If dialogImagem.ShowDialog() = DialogResult.OK Then

            Try
                With picImagem
                    .Image = Image.FromFile(dialogImagem.FileName)
                    .SizeMode = PictureBoxSizeMode.StretchImage
                End With
            Catch ex As Exception
                MessageBox.Show("O ficheiro n�o � uma imagem v�lida. Tem que escolher um ficheiro de imagem." & ex.Message, "Imagem inv�lida", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            End Try

        End If

        txtImagem.Text = dialogImagem.FileName.ToString

    End Sub

    Private Sub picImagem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles picImagem.Click

        Try
            Diagnostics.Process.Start(picImagem.ImageLocation)
        Catch ex As Exception
            Exit Sub
        End Try

    End Sub

    Private Sub chkEmprestado_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkEmprestado.CheckedChanged

        If chkEmprestado.Checked = False Then
            txtNomeEmprestado.Clear()
            txtDadosEmprestado.Clear()
            txtNomeEmprestado.Enabled = False
            txtDadosEmprestado.Enabled = False
        Else
            txtNomeEmprestado.Enabled = True
            txtDadosEmprestado.Enabled = True
        End If

    End Sub

    Private Sub txtProcurar_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtProcurar.TextChanged

        ListarDados()

    End Sub

    Private Sub cmbDrives_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbDrives.SelectedIndexChanged

        letraDrive = Microsoft.VisualBasic.Left(cmbDrives.Text, 2) 'retorna a letra da drive
        txtEtiqueta.Text = Microsoft.VisualBasic.Dir(letraDrive, FileAttribute.Volume) 'preenche a TextBox com o nome actual do volume escolhido

        infoDrives()
        txtEtiqueta.Focus()

    End Sub

    Private Sub lstDisco_ItemSelectionChanged(ByVal sender As Object, ByVal e As System.Windows.Forms.ListViewItemSelectionChangedEventArgs) Handles lstDisco.ItemSelectionChanged

        Dim colItem As ListView.SelectedListViewItemCollection
        Dim item As ListViewItem
        Dim str1 As String
        Dim str2 As String

        str1 = Nothing
        str2 = Nothing
        strCodDisco = Nothing
        strNome = Nothing
        colItem = lstDisco.SelectedItems

        For Each item In colItem
            str2 = item.SubItems(0).ToString
            'as 2 linhas seguintes removem todo o texto que est� a mais na string
            str2 = Microsoft.VisualBasic.Right(str2, Len(str2) - 18)
            str2 = Microsoft.VisualBasic.Left(str2, Len(str2) - 1)

            str1 = item.SubItems(4).ToString
            'as 2 linhas seguintes removem todo o texto que est� a mais na string
            str1 = Microsoft.VisualBasic.Right(str1, Len(str1) - 18)
            str1 = Microsoft.VisualBasic.Left(str1, Len(str1) - 1)
        Next

        strCodDisco = str1
        strNome = str2
        Sincronizar()

    End Sub

    Private Sub Sincronizar()

        Dim cmd2 As SqlClient.SqlCommand = New SqlClient.SqlCommand("SELECT Etiqueta,Descricao,Categoria,Imagem,Emprestado,NomeEmprestado,DadosEmprestado FROM Disco WHERE codDisco LIKE '" & strCodDisco & "'", SQL)
        Dim da As New SqlDataAdapter
        Dim ds As New DataSet
        Dim nome As String = Nothing
        Dim img As String

        Try
            SQL.Open()

            da.SelectCommand = cmd2
            da.Fill(ds)

            'as duas linhas seguintes formam uma string com o caminho da imagem a carregar
            nome = ds.Tables(0).Rows(0).Item("Imagem").ToString
            img = dirImg & nome & ".jpg"

            txtEtiqueta.Text = ds.Tables(0).Rows(0).Item("Etiqueta").ToString
            txtDescricao.Text = ds.Tables(0).Rows(0).Item("Descricao").ToString
            cmbCategoria.Text = ds.Tables(0).Rows(0).Item("Categoria").ToString
            picImagem.Image = Image.FromFile(img)
            chkEmprestado.Checked = ds.Tables(0).Rows(0).Item("Emprestado").ToString
            txtNomeEmprestado.Text = ds.Tables(0).Rows(0).Item("NomeEmprestado").ToString
            txtDadosEmprestado.Text = ds.Tables(0).Rows(0).Item("DadosEmprestado").ToString

        Catch ex As Exception
        Finally
            SQL.Close()
        End Try

        carregarXml(dirXml & txtEtiqueta.Text & ".xml", treeEstrutura)
        treeEstrutura.CollapseAll()

    End Sub

    Private Sub infoDrives()

        Dim drvCnt As Integer = Nothing
        Dim getDrvs() As String = Nothing
        Dim drvNum As Integer = Nothing
        Dim drvInfo As DriveInfo = Nothing
        Dim drvType As DriveType = Nothing
        Dim drvRootDir As DirectoryInfo = Nothing
        Dim TipoDrive As String = Nothing
        Dim mostrar As Boolean

        cmbDrives.Items.Clear() 'limpar os dados que estavam anteriormente na ComboBox

        getDrvs = IO.Directory.GetLogicalDrives()

        drvCnt = getDrvs.Length

        For drvNum = 0 To drvCnt - 1
            drvInfo = New DriveInfo(getDrvs(drvNum))
            drvType = drvInfo.DriveType

            mostrar = True

            Select Case drvType.ToString
                Case "Fixed"
                    TipoDrive = "Disco Local"
                Case "CDRom"
                    TipoDrive = "Drive de CD-ROM"
                Case "Network"
                    TipoDrive = "Drive de rede"
                Case "Removable"
                    TipoDrive = "Disco Amov�vel"
                Case "Unknown"
                    TipoDrive = "Tipo desconhecido"
            End Select

            If MostrarTodasAsDrivesToolStripMenuItem.Checked = False And drvType = DriveType.Fixed Or drvType = DriveType.Network Or drvType = DriveType.Unknown Then
                mostrar = False
            End If

            's� s�o mostradas as drives que cont�m dados
            If drvInfo.IsReady = True And mostrar = True Then
                cmbDrives.Items.Add(getDrvs.GetValue(drvNum) & "  -  " & TipoDrive)
            End If

        Next

    End Sub

    Private Sub txtImagem_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtImagem.TextChanged

        picImagem.ImageLocation = txtImagem.Text

    End Sub

    Private Sub lstDisco_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles lstDisco.LostFocus

        lstDisco.Clear()
        ListarDados()

    End Sub

    Private Sub criarXml()

        If letraDrive = Nothing Then
            Exit Sub
        End If

        Try

            'apagar todos os dados do documento XML
            xmlDoc.RemoveAll()

            xmlDoc.AppendChild(xmlDoc.CreateProcessingInstruction("xml", "version='1.0'"))
            createRootNode()
            xmlDoc.Save(dirXml & txtEtiqueta.Text & ".xml")
        Catch ex As Exception
            MessageBox.Show("Erro na grava��o da estrutura do disco: " & ex.ToString, "Erro na grava��o", MessageBoxButtons.OK)
        End Try


    End Sub

    Private Sub createRootNode()
        Try
            oElmntRoot = xmlDoc.CreateElement("myroot")
            oElmntRoot.SetAttribute("label", txtEtiqueta.Text)
            xmlDoc.AppendChild(oElmntRoot)
            loopNodes(oElmntRoot, letraDrive)

        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
    End Sub

    Private Function loopNodes(ByVal oElmntParent As XmlElement, ByVal strPath As String)

        Dim d As DirectoryInfo
        Dim ofs As New DirectoryInfo(strPath & "\")

        For Each d In ofs.GetDirectories
            Dim oElmntChild As XmlElement
            oElmntChild = xmlDoc.CreateElement("folder")
            oElmntChild.SetAttribute("label", d.Name)
            oElmntParent.AppendChild(oElmntChild)
            loopNodes(oElmntChild, strPath & "\" & d.Name)
        Next

        Dim oFile As FileInfo
        For Each oFile In ofs.GetFiles
            Dim oElmntLeaf As XmlElement
            oElmntLeaf = xmlDoc.CreateElement("file")
            oElmntLeaf.SetAttribute("label", oFile.Name)
            oElmntLeaf.SetAttribute("filePath", strPath)
            oElmntLeaf.SetAttribute("bytesSize", CType(oFile.Length, String))
            oElmntParent.AppendChild(oElmntLeaf)
        Next

    End Function

    Private Sub carregarXml(ByVal file_name As String, ByVal treeEstrutura As TreeView)

        xmlDoc.Load(file_name)
        treeEstrutura.Nodes.Clear()
        AddTreeViewChildNodes(treeEstrutura.Nodes, xmlDoc.DocumentElement)

    End Sub

    Private Sub AddTreeViewChildNodes(ByVal parent_nodes As TreeNodeCollection, ByVal xml_node As XmlNode)

        For Each child_node As XmlNode In xml_node.ChildNodes
            Dim new_node As TreeNode = parent_nodes.Add(child_node.Attributes("label").InnerText)

            AddTreeViewChildNodes(new_node.Nodes, child_node)
            If new_node.Nodes.Count = 0 Then new_node.EnsureVisible()
        Next child_node

    End Sub

#End Region



#Region " Inserir Registos "

    Private Sub btnInserir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnInserir.Click

        Dim nomeImg As String = Nothing
        Dim Query1 As String = "INSERT INTO Disco (Etiqueta,Descricao,Categoria,Imagem,Emprestado,NomeEmprestado,DadosEmprestado) VALUES (@Etiqueta,@Descricao,@Categoria,@Imagem,@Emprestado,@NomeEmprestado,@DadosEmprestado)"
        Dim cmd As SqlClient.SqlCommand = New SqlClient.SqlCommand(Query1, SQL)

        Cursor.Current = Cursors.WaitCursor

        'verifica��o do campo Etiqueta
        If txtEtiqueta.Text.Length = 0 Then
            MessageBox.Show("O campo Etiqueta � obrigat�rio para inserir um item na base de dados!!", "Erro a inserir o item", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            txtEtiqueta.Focus()
            Exit Sub
        End If

        'verifica��o da imagem relacionada ao item a inserir
        If picImagem.Image Is Nothing Then
            nomeImg = "semimagem"
        Else
            Dim bm As Bitmap = picImagem.Image

            nomeImg = txtEtiqueta.Text
            bm.Save(dirImg & nomeImg & ".jpg", ImageFormat.Jpeg)
            bm.Dispose()
        End If

        If letraDrive = Nothing Then
            MessageBox.Show("Tem que seleccionar uma drive de onde recolher dados!", "Erro na inser��o", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Exit Sub
        Else
            criarXml() 'cria o ficheiro XML que cont�m a estrutura do volume
        End If

        cmd.Parameters.AddWithValue("@Etiqueta", txtEtiqueta.Text)
        cmd.Parameters.AddWithValue("@Descricao", txtDescricao.Text)
        cmd.Parameters.AddWithValue("@Categoria", cmbCategoria.Text)
        cmd.Parameters.AddWithValue("@Imagem", nomeImg)
        cmd.Parameters.AddWithValue("@Emprestado", chkEmprestado.Checked.ToString)
        cmd.Parameters.AddWithValue("@NomeEmprestado", txtNomeEmprestado.Text)
        cmd.Parameters.AddWithValue("@DadosEmprestado", txtDadosEmprestado.Text)

        Try
            SQL.Open()
            cmd.ExecuteNonQuery()
            ListarDados()
        Catch ex As Exception
            MessageBox.Show("Erro na inser��o dos dados: " & ex.Message)
        Finally
            SQL.Close()
            Cursor.Current = Cursors.Default
        End Try

        'limpar os controlos
        txtEtiqueta.Clear()
        txtDescricao.Clear()
        txtImagem.Clear()
        cmbCategoria.Text = ""
        cmbDrives.Text = ""
        picImagem.Image = Nothing
        chkEmprestado.Checked = False
        txtNomeEmprestado.Clear()
        txtDadosEmprestado.Clear()

    End Sub

#End Region

#Region " Apagar Registos "

    Private Sub btnApagar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnApagar.Click

        Dim cmd As SqlClient.SqlCommand = New SqlClient.SqlCommand("DELETE FROM Disco WHERE codDisco = '" & strCodDisco & "'", SQL)

        If strCodDisco = Nothing Then
            MessageBox.Show("� necess�rio escolher um registo para apagar.", "Erro na remo��o", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End If

        Try

            SQL.Open()
            cmd.ExecuteNonQuery()
            ListarDados()
            strCodDisco = Nothing

        Catch ex As Exception
            MessageBox.Show("Erro na remo��o de registos: " & ex.Message)
        Finally
            'limpar os controlos
            txtEtiqueta.Clear()
            txtDescricao.Clear()
            txtImagem.Clear()
            cmbCategoria.Text = ""
            cmbDrives.Text = ""
            picImagem.Image = Nothing
            chkEmprestado.Checked = False
            txtNomeEmprestado.Clear()
            txtDadosEmprestado.Clear()
            treeEstrutura.Nodes.Clear()
            SQL.Close()
        End Try

    End Sub
#End Region

#Region " Listar Dados "

    Private Sub ListarDados()

        Try
            lstDisco.Clear()
            ds.Reset()
            preencherDataset()
        Catch ex As Exception
            lstDisco.Clear()
            MessageBox.Show("Erro no processamento do DataSet: " & ex.Message)
        End Try

    End Sub

    Private Sub preencherDataset()
        Me.da = New SqlDataAdapter("SELECT Etiqueta,Descricao,Categoria,Emprestado,codDisco FROM Disco WHERE Etiqueta LIKE '%" & txtProcurar.Text & "%'", SQL)
        da.Fill(Me.ds)
        Preenchimento()
    End Sub

    Private Sub Preenchimento()
        Try
            Me.lstDisco.Columns.Clear()
            Me.lstDisco.Items.Clear()
            Me.preencherListView()
        Catch ex As Exception
            Me.lstDisco.Clear()
            MessageBox.Show("Erro no preenchimento da ListView: " & ex.Message, ex.GetType().ToString())
        End Try
    End Sub

    Private Sub preencherListView()

        Dim tabela As DataTable = ds.Tables(0)
        Dim str(Me.ds.Tables(0).Columns.Count) As String

        'aqui s�o criadas as colunas da ListView
        lstDisco.Columns.Add("Etiqueta", 120, HorizontalAlignment.Left)
        lstDisco.Columns.Add("Descri��o", 240, HorizontalAlignment.Left)
        lstDisco.Columns.Add("Categoria", 75, HorizontalAlignment.Left)
        lstDisco.Columns.Add("Empr.", 55, HorizontalAlignment.Center)
        lstDisco.Columns.Add("ID", 30, HorizontalAlignment.Center)

        'inclui DataRows ao ListView
        Dim linha As DataRow
        For Each linha In tabela.Rows
            For col As Integer = 0 To Me.ds.Tables(0).Columns.Count - 1
                ' este Select Case muda os valores retornados pela coluna Emprestado
                ' em vez de True e False, passa a retornar Sim ou N�o
                Select Case linha(col).ToString
                    Case "True"
                        str(col) = "Sim"
                    Case "False"
                        str(col) = "N�o"
                    Case Else
                        ' aqui retorna-se o valor exactamente como na BD
                        str(col) = linha(col).ToString()
                End Select
            Next
            Dim item As New ListViewItem(str)
            Me.lstDisco.Items.Add(item)

        Next
    End Sub

#End Region

End Class