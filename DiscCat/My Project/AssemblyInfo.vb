﻿Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices

' General Information about an assembly is controlled through the following 
' set of attributes. Change these attribute values to modify the information
' associated with an assembly.

' Review the values of the assembly attributes

<Assembly: AssemblyTitle("DiscCat")> 
<Assembly: AssemblyDescription("Este programa é gratuito e open-source. Se tiver alguma dúvida ou necessitar do código fonte contacte o autor no endereço: defik@portugalmail.pt")> 
<Assembly: AssemblyCompany("n/a")> 
<Assembly: AssemblyProduct("DiscCat")> 
<Assembly: AssemblyCopyright("Copyright _MsK_©  2008")> 
<Assembly: AssemblyTrademark("")> 

<Assembly: ComVisible(False)>

'The following GUID is for the ID of the typelib if this project is exposed to COM
<Assembly: Guid("69a58cfa-b195-41bc-936b-aee13e5804d2")> 

' Version information for an assembly consists of the following four values:
'
'      Major Version
'      Minor Version 
'      Build Number
'      Revision
'
' You can specify all the values or you can default the Build and Revision Numbers 
' by using the '*' as shown below:
' <Assembly: AssemblyVersion("1.0.*")> 

<Assembly: AssemblyVersion("1.0.0.0")> 
<Assembly: AssemblyFileVersion("1.0.0.0")> 
